const puzzleImg = document.getElementById('puzzle')
const canvas = document.querySelector('canvas')

const ctx = canvas.getContext('2d')
ctx.strokeStyle = "#f00"
ctx.fillStyle = "#f00"
ctx.lineWidth = 2
ctx.font = '16px sans'

const 
  left = 10,
  gridTop = 10,
  right = 745,
  bottom = 748,
  squareWidth = (right - left) / 17,
  squareHeight = (bottom - gridTop) / 17,
  triangles = []

let points = [], closestCorner;

const redraw = () => {
  ctx.clearRect(0, 0, canvas.width, canvas.height)
  ctx.beginPath()
  triangles.forEach(([pt1, pt2, pt3]) => {
    ctx.moveTo(pt1.x, pt1.y)
    ctx.lineTo(pt2.x, pt2.y)
    ctx.lineTo(pt3.x, pt3.y)
    ctx.lineTo(pt1.x, pt1.y)
  })
  if (points.length > 1) {
    const [first, ...rest] = points
    ctx.moveTo(first.x, first.y)
    rest.forEach(pt => {
      ctx.lineTo(pt.x, pt.y)
    })
  }
  ctx.stroke()
  if (closestCorner) {
    ctx.beginPath()
    ctx.ellipse(closestCorner.x, closestCorner.y, 5, 5, 1, 1, 100)
    ctx.stroke()
  }
}


const findClosestCorner = ({x,y}) => {

  const column = Math.floor((x - left) / squareWidth)
  const columnLeft = column * squareWidth + left

  const row = Math.floor((y - gridTop) / squareHeight)
  const rowTop = row * squareHeight + gridTop
  
  // const result = {
  //   x: x - columnLeft < squareWidth / 2 ? columnLeft : columnLeft + squareWidth,
  //   y: y - rowTop < squareHeight / 2 ? rowTop: rowTop + squareHeight
  // }
  // return result
  const rowBits = y - rowTop < squareHeight / 2
  ? { y: rowTop, row: row }
  : { y: rowTop + squareHeight, row: row + 1 }

  const colBits = x - columnLeft < squareWidth / 2
  ? { x: columnLeft, col: column }
  : { x: columnLeft + squareWidth, col: column + 1 }

  return {
    ...rowBits,
    ...colBits
  }
}


const addPoint = pt => {
  if (points.length === 0) {
    points.push(pt)
  } else if (points.length === 1) {
    points.push(pt)
  } else {
    points.push(pt)
    triangles.push(points)
    points = []
  }
  redraw()
}

window.addEventListener('keypress', e => {
  if (e.key === 'u') {
    if (points.length > 0) {
      points = []
    } else {
      triangles.pop()
    }
    redraw()
  }
})

const addPointToClosestCorner = pt => addPoint(findClosestCorner(pt))

puzzleImg.addEventListener('click', e => {
  const {
    x: offsetX,
    y: offsetY
  } = e.target.getBoundingClientRect()
  const { clientX, clientY } = e
  addPointToClosestCorner({ 
    x: clientX - offsetX, 
    y: clientY - offsetY
  })
})

puzzleImg.addEventListener('mousemove', e => {
  const {
    x: offsetX,
    y: offsetY
  } = e.target.getBoundingClientRect()
  const { clientX, clientY } = e
  closestCorner = findClosestCorner({ 
    x: clientX - offsetX, 
    y: clientY - offsetY
  })
  redraw()
  if (points.length === 2) {
    const area = getArea([...points, closestCorner])
    ctx.fillText(String(area), clientX, clientY)
  }
})

const getArea = (trianglePts) => {
  const top = Math.min(...trianglePts.map(x => x.row))
  const bottom = Math.max(...trianglePts.map(x => x.row))
  const left = Math.min(...trianglePts.map(x => x.col))
  const right = Math.max(...trianglePts.map(x => x.col))

  const area = (bottom - top) * (right - left) / 2
  return area
}


